A brochure about GLib
=====================

**Warning:** the brochure is in the process of being rewritten for GLib only.
The first versions were about GLib and GTK (for GTK 3 but it is now obsolete).

Some chunks of text come from the [gtk.org](https://gtk.org/) website or from
the reference documentation.

A PDF is available at: (TODO)
