\documentclass[a4paper,notumble]{leaflet}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage{graphicx}

% PDF properties and links
\usepackage{hyperref}
\hypersetup{
  pdfauthor   = {Sébastien Wilmet},
  pdftitle    = {GLib Brochure},
  pdfcreator  = {TeX},
  pdfproducer = {TeX},
  colorlinks  = false,
  pdfborder   = 0 0 0
}

\title{GLib}
\date{}
\author{}

\begin{document}

\maketitle
\thispagestyle{empty}

GLib is a multi-platform C library. It permits to write C code more comfortably.

GLib is free/libre software. The licensing terms for GLib, the GNU LGPL, allow it to be used by all developers without any license fees or royalties.

% GTK+ is written in C but has been designed from the ground up to support a wide range of languages, not only C/C++. Using GTK+ from languages such as Python and JavaScript provides an effective method of application development.
%
% GTK+ has been created in 1996 for the GIMP --- the GNU Image Manipulation Program --- but quickly evolved into a general-purpose library used by a large number of applications including the GNU project's GNOME desktop.

\vfill

\begin{center}
  \includegraphics[width=2cm]{images/gnome-logo.pdf}

  \bigskip
  \url{https://www.gtk.org/}\par
  \url{https://www.gnome.org/}
\end{center}

% \pagebreak
%
% GLib, a low-level core library that forms the basis of GTK+. It provides data structure handling for C, portability wrappers and interfaces for such run-time functionality as an event loop, threads, dynamic loading, an object system (GObject) and high-level input/output APIs (GIO).

\pagebreak
\section{GLib -- the Core Library}

Firstly, GLib provides common data structures:
\begin{itemize}
  \item Single and doubly linked lists.
  \item Hash tables.
  \item Balanced binary trees.
  \item N-ary trees: trees of data with any number of branches.
  \item Strings with text buffers which grow automatically as text is added.
  \item Arrays of arbitrary elements which grow automatically as elements are added.
  \item \texttt{GVariant}, a generic data type that stores a value along with information about the type of that value.
  \item And a few other data structures.
\end{itemize}

\bigskip
GLib also contains loads of utilities:
\begin{itemize}
  \item String and Unicode manipulation.
  \item Date and time functions.
  \item A command-line option parser.
  \item Perl-compatible regular expressions.
  \item An XML parser.
  \item A key-value file parser, for .ini-like config files.
  \item A unit-test framework.
  \item And many other utilities.
\end{itemize}

% TODO: talk about GError,
% Threads and asynchronous communication between threads.

\pagebreak
\section{Semi-OOP Style}

In a lot of cases, GLib Core follows a semi-object-oriented programming style. Each ``class'' has:
\begin{itemize}
  \item A \texttt{struct}.
  \item One or more constructors. A constructor is a function often named \texttt{new()} that dynamically allocates memory for the \texttt{struct} and returns the pointer (the \textit{self pointer}). Can be used to create several ``objects'' (instances) that exist at the same time.
  \item Methods: functions that take a self pointer as parameter to access the struct fields of the object.
  \item One or more destructors. A destructor is a function that frees the memory and other resources that were allocated for an object.
\end{itemize}

\section{Reference-Counting}

For most semi-object-oriented classes in GLib Core, memory management is handled by \textit{reference counting}. A constructor returns an object with a reference count of 1. There are \texttt{ref()} and \texttt{unref()} functions to increment and decrement the reference count of an object. When the reference count decreases to 0 the object is destroyed.

\section{Naming Conventions}

A semi-object-oriented class in GLib follows a predictable naming convention:

\texttt{NamespaceClassname} for the struct;\par
\texttt{namespace\_classname\_methodname()} for functions.

The namespace of GLib is just the letter ``G''. Example:

\begin{tabular}{@{}ll@{}}
\texttt{GHashTable} &  \\
\texttt{g\_hash\_table\_new()} & \texttt{g\_hash\_table\_insert()} \\
\texttt{g\_hash\_table\_ref()} & \texttt{g\_hash\_table\_lookup()} \\
\texttt{g\_hash\_table\_unref()} & \texttt{...} \\
\end{tabular}

\pagebreak
\section{Event-Driven Programming}

GLib has been created with interactive programs in mind. An interactive program listens to some \textit{events} and reacts to them.

Examples of events: key presses, mouse clicks, or touch gestures (especially useful for GUI applications), a USB stick inserted, a second monitor connected, or a printer low on paper (useful for daemons that respond to hardware changes), messages coming from the network, messages coming from daemons or other processes --- you get the idea.

To support the event-driven programming paradigm, GLib provides a \textit{main event loop} abstraction. An event loop listens some sources of events, that can come from file descriptors (plain files, pipes or sockets), timeouts, or other custom sources. A priority is associated with each source of events. When an event arrives, the event loop dispatches it to the program. The event can then be taken into account, either in the same thread or another thread.

\begin{center}
  \includegraphics[width=6cm]{images/event-loop.pdf}\\[0.2cm]
  A main event loop.
\end{center}

To take into account events, the program attaches \textit{callback functions} (pointers to functions).

The \texttt{main()} function typically creates a GApplication instance and then calls \texttt{g\_application\_run()} (GApplication is provided by GIO; GLib Core alone is a bit low-level for that task). \texttt{g\_application\_run()} blocks the current thread to run the main event loop. The function returns when an event to quit the program is received (for a GUI app, this happens when closing its last window).

\pagebreak
\section{GObject -- an Object System}

Most modern programming languages come with their own native object systems and additional fundamental algorithmic language constructs. Just as GLib serves as an implementation of such fundamental types and algorithms (linked lists, hash tables and so forth), GObject provides an implementation of a flexible, extensible, and intentionally easy to map (into other languages) object-oriented framework for C.

All common object-oriented concepts are present: inheritance, interfaces, virtual/overridable methods, abstract classes, constructors, destructors, and so on. Memory management is handled by reference counting. GObject also provides a powerful \textit{signal system}: you can create your own signals and connect callback functions to it. It is a great way to decouple classes since the object sending the signal doesn't know who receive it. Another peculiarity of GObject is a \textit{property}, which is basically an object attribute surmounted by a \texttt{notify} signal that is sent when its value changes.

% TODO: section about GObject Introspection.

\pagebreak
\section{GIO -- Input/Output}

GIO is striving to provide a modern, easy-to-use Virtual File System (VFS) API that sits at the right level in the library stack. GIO also contains other generally useful APIs for desktop applications (such as networking and D-Bus support). It was created to provide an API that is so good that developers prefer it over raw POSIX calls. Among other things that means using GObject. It also means not cloning the POSIX API, but providing higher-level, document-centric interfaces.

The abstract file system model of GIO consists of a number of interfaces and base classes for I/O and files. Then there is a number of stream classes, similar to the input and output stream hierarchies that can be found in frameworks like Java. There is a framework for storing and retrieving application settings. There is support for network programming, including connectivity monitoring, name resolution, lowlevel socket APIs and highlevel client and server helper classes. There is support for connecting to the D-Bus inter-process communication system: sending and receiving messages, owning and watching bus names, and making objects available on the bus. Beyond these, GIO provides: file monitoring; utility classes to implement asynchronous and/or cancellable operations; an easy-to-use API to launch and interact with child processes; and more.

By using the same GIO API, an application can support remote files in addition to local files. The local case is implemented by GIO, while implementations for various network file systems are provided by the GVfs package as loadable modules. Another design choice is to move backends out-of-process, which minimizes the dependency bloat and makes the whole system more robust. The backends are not included in GIO, but in the separate GVfs package. GVfs also contains a daemon which spawn further mount daemons for each individual connection.

\end{document}
